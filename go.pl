#!/usr/bin/env perl
use strict;
use warnings;

my @dir = @ARGV;
foreach my $work (@dir) {
    print "$work\n";
    system "perl cut.pl $work > log-${work}.txt";
}
