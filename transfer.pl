#!/usr/bin/env perl
use strict;
use warnings;
$ENV{SAC_DISPLAY_COPYRIGHT} = 0;

@ARGV == 1 or die "Usage: perl $0 dir\n";
my ($dir) = @ARGV;

chdir $dir;

# 去仪器响应
open(SAC, "| sac") or die "Error in opening sac\n";
print SAC "wild echo off\n";
foreach (glob "*.SAC") {
    print SAC "r $_\n";
    print SAC "rglitches; rmean; rtrend; taper \n";
    #print SAC "bp c 0.5 4.9 n 2 p 1\n";
#    print SAC "mul 1e-7\n"; 
    print SAC "w over\n";
}
print SAC "q\n";
close(SAC);

chdir "..";
