#!/usr/bin/env perl
use strict;
use warnings;
use List::Util qw(min);
use List::Util qw(max);
$ENV{SAC_DISPLAY_COPYRIGHT} = 0;

@ARGV == 1 or die "Usage: perl $0 dir\n";
my ($dir) = @ARGV;

chdir $dir or die;

my $b = 0;
my $e = 43200 + 100;

open(SAC, "| sac") or die "Error in opening SAC\n";
print SAC "wild echo off\n";
print SAC "cuterr fillz\n";
print SAC "cut $b $e\n";
foreach my $file (glob "*.SAC") {
    my ($net, $sta, $loc, $chn) = (split /\./, $file)[6..9];
    $chn = lc substr ($chn, 2, 1);
    print SAC "r $file\n";
    print SAC "w $sta.$chn\n";
}
print SAC "q\n";
close(SAC);
mkdir "../${dir}a";
system "mv *.[enz] ../${dir}a";

$b = 43200;
$e = 86400 + 100;

open(SAC, "| sac") or die "Error in opening SAC\n";
print SAC "wild echo off\n";
print SAC "cuterr fillz\n";
print SAC "cut $b $e\n";
foreach my $file (glob "*.SAC") {
    my ($net, $sta, $loc, $chn) = (split /\./, $file)[6..9];
    $chn = lc substr ($chn, 2, 1);
    print SAC "r $file\n";
    print SAC "w $sta.$chn\n";
}
print SAC "q\n";
close(SAC);
mkdir "../${dir}b";
system "mv *.[enz] ../${dir}b";

chdir "..";

