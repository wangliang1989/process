#!/usr/bin/env perl
use strict;
use warnings;
use POSIX qw(strftime);
$ENV{SAC_DISPLAY_COPYRIGHT} = 0;

@ARGV == 1 or die "Usage: perl $0 dir\n";
my ($dir) = @ARGV;

chdir $dir;

my $year = substr ($dir, 0, 4);
my $month = substr ($dir, 4, 2);
my $day = substr ($dir, 6, 2);
my $hour = 0;
my $minute = 0;
my $sec = 0;
my $msec = 0;

# 计算发震日期是一年中的第几天
my $jday = strftime("%j", $sec, $minute, $hour, $day, $month-1, $year-1900);

open(SAC, "| sac") or die "Error in opening SAC\n";
print SAC "wild echo off\n";
foreach (glob "*.SAC") {
    print SAC "r $_\n";
    print SAC "ch o gmt $year $jday $hour $minute $sec $msec\n";
    print SAC "ch allt (0 - &1,o&) iztype IO\n";
    print SAC "wh\n";
}
print SAC "q\n";
close(SAC);

chdir "..";
