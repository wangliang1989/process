#!/usr/bin/env perl
use strict;
use warnings;
use List::Util qw(min max);

@ARGV == 1 or die "Usage: perl $0 dirname\n";
my ($dir) = @ARGV;

chdir $dir;

open(SAC, "|sac") or die "Error in opening sac\n";
print SAC "r *E.?.SAC\n";
print SAC "ch cmpinc 90 cmpaz 90\n";
print SAC "wh\n";
print SAC "r *N.?.SAC\n";
print SAC "ch cmpinc 90 cmpaz 0\n";
print SAC "wh\n";
print SAC "r *Z.?.SAC\n";
print SAC "ch cmpinc 0 cmpaz 0\n";
print SAC "wh\n";
print SAC "q\n";
close(SAC);

chdir "..";
